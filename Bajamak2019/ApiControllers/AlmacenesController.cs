﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Data;
using Bajamak2019.Extras.Dto.Almacen;
using Bajamak2019.Extras.Dto.Anden;
using Bajamak2019.Extras.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Bajamak2019.ApiControllers
{
    [Produces("application/json")]
    [Route("api/Almacenes")]
    public class AlmacenesController : Controller
    {
        private readonly IAlmacen _almacenService;

        public AlmacenesController(IAlmacen almacenService)
        {
            _almacenService = almacenService;
        }

        // GET: api/Almacenes
        [HttpGet]
        public IEnumerable<AlmacenDto> TraeListaAlmacenes()
        {
            return _almacenService.TraeListaAlmacenes();
        }

        //// GET api/<controller>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
