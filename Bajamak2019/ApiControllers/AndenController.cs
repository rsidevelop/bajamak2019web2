﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Extras.Dto.Anden;
using Bajamak2019.Extras.Services;
using Bajamak2019.Extras.ViewModels.Anden;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Bajamak2019.ApiControllers
{
    [Produces("application/json")]
    [Route("api/Andenes")]
    public class AndenController : Controller
    {
        private readonly IAnden _andenService;

        public AndenController(IAnden andenService)
        {
            _andenService = andenService;
        }

        // GET: api/Andenes
        [HttpGet]
        public IEnumerable<AndenDto> TraeListaAndenes()
        {
            return _andenService.TraeListaTodosAndenes();
        }
        //[HttpGet]
        //public IEnumerable<AndenDto> TraeListaAndenes([FromBody] AndenViewModel request)
        //{
        //    return _andenService.TraeListaAndenes(request);
        //}

        // GET: api/Andenes/5
        [HttpGet("{id}", Name = "Get")] //https://localhost:44308/api/Andenes/2?TipoAnden=comentariolol&otra=lolazo
        public IEnumerable<AndenDto> Get(int id, [FromQuery] string TipoAnden)
        {
            AndenViewModel request = new AndenViewModel() { IdAlmacen=id, TipoAnden = TipoAnden};
            return _andenService.TraeListaAndenes(request);
        }
        //[HttpGet("{id}", Name = "Get")] //https://localhost:44308/api/Andenes/2?TipoAnden=comentariolol&otra=lolazo
        //public string Get(int id, [FromQuery] string TipoAnden)
        //{
        //    return "value";
        //}
        //[HttpGet("{id}", Name = "Get")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST: api/Anden
        //[HttpPost]
        //public void Post([FromBody] string value)
        //{
        //}

        //// PUT: api/Anden/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody] string value)
        //{
        //}

        //// DELETE: api/ApiWithActions/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}




// GET: api/Andenes/5
//[HttpGet("{id}", Name = "Get")] //https://localhost:44308/api/Andenes/2?TipoAnden=comentariolol&otra=lolazo
//public string Get(int id, [FromQuery] string TipoAnden, [FromQuery] string otra)
//{
//    return "value";
//}