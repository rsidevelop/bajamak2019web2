﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Bajamak2019.Extras.Dto.Inventario;
using Bajamak2019.Extras.Services;

namespace Bajamak2019.ApiControllers
{
    [Produces("application/json")]
    [Route("api/Inventario")]
    [ApiController]
    public class InventarioController : Controller
    {
        private readonly IInventario _inventarioService;

        public InventarioController(IInventario inventarioService)
        {
            _inventarioService = inventarioService;
        }

        // GET: api/Inventario
        [HttpGet]
        public IEnumerable<InventarioDto> TraeListaInventario()
        {
            return _inventarioService.TraeListaInventario();
        }
    }
}
