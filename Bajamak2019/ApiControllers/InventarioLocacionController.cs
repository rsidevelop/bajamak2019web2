﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Extras.Dto.InventarioLocacion;
using Bajamak2019.Extras.Dto.Locacion;
using Bajamak2019.Extras.Services;
using Bajamak2019.Extras.ViewModels.InventarioLocacion;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Bajamak2019.ApiControllers
{
    [Produces("application/json")]
    [Route("api/inventariolocacion")]
    public class InventarioLocacionController : Controller
    {
        private readonly IInventarioLocacion _inventarioLocacionService;

        public InventarioLocacionController(IInventarioLocacion inventarioLocacionService)
        {
            _inventarioLocacionService = inventarioLocacionService;
        }

        // GET: api/InventarioLocacion
        [HttpGet]
        IEnumerable<InventarioLocacionDto> TraeListaLocacionesInventario([FromBody] InventarioLocacionViewModel request)
        {
            return _inventarioLocacionService.TraeListaLocacionesInventario(request);
        }



        // GET: api/InventarioLocacion/idAlmacen
        [HttpGet("{idAlmacen}")]
        public IEnumerable<LocacionDto> TraeListaLocacionesDisponibles(int idAlmacen, int idNumeroParte, string MoveType, int Cantidad)
        {
            if (MoveType == "Entrada")//https://localhost:44308/api/inventariolocacion/1?MoveType=Entrada                 Ejemplo Entrada
            {
                return _inventarioLocacionService.TraeListaLocacionesDisponibles(idAlmacen);
            }
            else if(MoveType == "Salida")//https://localhost:44308/api/inventariolocacion/1?idNumeroParte=1&MoveType=Salida&Cantidad=0        Ejemplo Salida
            {
                return _inventarioLocacionService.TraeListaLocacionesPorParte(idAlmacen, idNumeroParte, Cantidad);
            }
            return new List<LocacionDto>();
        }
         

        //// GET: api/InventarioLocacion/idAlmacen
        //[HttpGet("{idAlmacen}")]
        //public IEnumerable<LocacionDto> TraeListaLocacionesDisponibles(int idAlmacen)
        //{
        //    return _inventarioLocacionService.TraeListaLocacionesDisponibles(idAlmacen);
        //}



        // GET: api/Andenes/5
        //[HttpGet("{id}", Name = "Get")] //https://localhost:44308/api/Andenes/2?TipoAnden=comentariolol&otra=lolazo
        //public IEnumerable<AndenDto> Get(int id, [FromQuery] string TipoAnden)
        //{
        //    AndenViewModel request = new AndenViewModel() { IdAlmacen = id, TipoAnden = TipoAnden };
        //    return _andenService.TraeListaAndenes(request);
        //}

    }
}