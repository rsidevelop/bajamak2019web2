﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Bajamak2019.Extras.Services;

namespace Bajamak2019.ApiControllers
{
    [Produces("application/json")]
    [Route("api/Montacargas")]
    [ApiController]
    public class MontacargasController : Controller
    {
        private readonly IMontacargas _montacargasService;

        public MontacargasController(IMontacargas montacargasService)
        {
            _montacargasService = montacargasService;
        }

        // GET: api/Montacargas
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Montacargas/5
        [HttpGet("{id}", Name = "GetMonta")]
        public int Get(int id)
        {
            return _montacargasService.TraeMontacargasAsignado(id);
        }
    }
}

