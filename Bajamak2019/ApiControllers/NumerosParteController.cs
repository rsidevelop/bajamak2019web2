﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Extras.Dto.NumeroParte;
using Bajamak2019.Extras.Services;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Bajamak2019.ApiControllers
{
    [Produces("application/json")]
    [Route("api/NumerosParte")]
    public class NumerosParteController : Controller
    {
        private readonly INumeroParte _numeroParteService;

        public NumerosParteController(INumeroParte numeroParteService)
        {
            _numeroParteService = numeroParteService;
        }

        // GET: api/NumerosParte
        [HttpGet]
        public IEnumerable<NumeroParteDto> TraeListaNumerosParte()
        {
            return _numeroParteService.TraeListaNumerosParte();
        }

        //// GET api/<controller>/5
        //[HttpGet("{id}")]
        //public string Get(int id)
        //{
        //    return "value";
        //}

        //// POST api/<controller>
        //[HttpPost]
        //public void Post([FromBody]string value)
        //{
        //}

        //// PUT api/<controller>/5
        //[HttpPut("{id}")]
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        //// DELETE api/<controller>/5
        //[HttpDelete("{id}")]
        //public void Delete(int id)
        //{
        //}
    }
}
