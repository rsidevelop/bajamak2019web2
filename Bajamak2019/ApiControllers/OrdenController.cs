﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Extras.Dto.Almacen;
using Bajamak2019.Extras.Dto.Orden;
using Bajamak2019.Extras.Messages;
using Bajamak2019.Extras.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Bajamak2019.ApiControllers
{
    [Produces("application/json")]
    [Route("api/Ordenes")]
    public class OrdenController : Controller
    {
        private readonly IOrden _ordenService;
        public OrdenController(IOrden ordenService)
        {
            _ordenService = ordenService;
        }

        // GET: api/Ordenes
        [HttpGet]
        public IEnumerable<OrdenDto> TraeListaOrdenes()
        {
            var Ordenes = _ordenService.TraeListaOrdenes().OrderByDescending(x => x.IdOrden).Take(10);
            return Ordenes;
        }



        // GET: api/Ordenes/5
        [HttpGet("{id}", Name = "GetOrdenPorAlmacen")] //https://localhost:44308/api/Andenes/2?TipoAnden=comentariolol&otra=lolazo
        public IEnumerable<OrdenDto> Get(int id)
        {
            return _ordenService.TraeListaOrdenes(id).OrderByDescending(x => x.IdOrden).Take(10); ;
        }

        // POST api/Ordenes
        [HttpPost]
        public string Post([FromBody]NuevaOrdenRequest ordenRequest)
        {
            string response = _ordenService.GuardarOrden(ordenRequest);
            return response;
        }


        //json that works with postman

        //{"idOrden":2,"cantidad":500.00,"tipoMovimiento":"Salida","comentario":"comentario","fechaRegistro":"2019-09-24T00:00:00-07:00","fechaAtencion":"2022-07-24T00:00:00","idTag":"","numeroParte":{"idNumeroParte":1,"codigo":"000001","descripcion":"Rollo Etiquetas A04"},"montacargas":{"idMontacargas":1,"almacen":{"idAlmacen":1,"descripcion":"Almacén Tijuana"},"descripcion":"Montacargas 01"},"anden":{"idAnden":1,"descripcion":"Entregas 01"},"locacion":{"idLocacion":1,"descripcion":"A01","nivel":"1","posicion":1,"nombreAntena":"A01","almacen":{"idAlmacen":1,"descripcion":"Almacén Tijuana"}},"status":"ABIERTO"}




    //**************************This Worked as postman (body, raw, json)
    //[HttpPost]
    //public void Post([FromBody]AlmacenDto almacenDto)
    //{
    //    //cargando
    //}

    //{
    //   Descripcion: 'Andrew',
    //   LastName: 'Lock',
    //   IdAlmacen: 31
    //}

}
}