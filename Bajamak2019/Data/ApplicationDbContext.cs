﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Models;
using Microsoft.EntityFrameworkCore;

namespace Bajamak2019.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        { }

        public DbSet<Almacen> Almacenes { get; set; }
        public DbSet<Anden> Andenes { get; set; }
        public DbSet<Antena> Antenas { get; set; }
        public DbSet<Inventario> Inventario { get; set; }
        public DbSet<InventarioLocacion> InventarioLocacion { get; set; }
        public DbSet<Locacion> Locaciones { get; set; }
        public DbSet<Montacargas> Montacargas { get; set; }
        public DbSet<NumeroParte> NumerosParte { get; set; }
        public DbSet<Orden> Ordenes { get; set; }
    }
}
