﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bajamak2019.Migrations
{
    public partial class InitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "bmk_almacenes_alm",
                columns: table => new
                {
                    almIdAlmacen = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    almDescripcion = table.Column<string>(maxLength: 150, nullable: false),
                    almActivo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_almacenes_alm", x => x.almIdAlmacen);
                });

            migrationBuilder.CreateTable(
                name: "bmk_antenas_ant",
                columns: table => new
                {
                    antIdAntena = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    antNombre = table.Column<string>(maxLength: 15, nullable: false),
                    antIdAnden = table.Column<int>(nullable: false),
                    andTipoAntena = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_antenas_ant", x => x.antIdAntena);
                });

            migrationBuilder.CreateTable(
                name: "bmk_locaciones_loc",
                columns: table => new
                {
                    locIdLocacion = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    locDescripcion = table.Column<string>(maxLength: 50, nullable: false),
                    locNivel = table.Column<string>(maxLength: 10, nullable: false),
                    locPosicion = table.Column<int>(nullable: false),
                    locNombreAntena = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_locaciones_loc", x => x.locIdLocacion);
                });

            migrationBuilder.CreateTable(
                name: "bmk_numerosparte_np",
                columns: table => new
                {
                    npIdNumeroParte = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    npCodigo = table.Column<string>(maxLength: 50, nullable: false),
                    npDescripcion = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_numerosparte_np", x => x.npIdNumeroParte);
                });

            migrationBuilder.CreateTable(
                name: "bmk_andenes_and",
                columns: table => new
                {
                    andIdAnden = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    almIdAlmacen = table.Column<int>(nullable: true),
                    andTipoAnden = table.Column<string>(maxLength: 20, nullable: false),
                    andDescripcion = table.Column<string>(maxLength: 150, nullable: false),
                    andActivo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_andenes_and", x => x.andIdAnden);
                    table.ForeignKey(
                        name: "FK_bmk_andenes_and_bmk_almacenes_alm_almIdAlmacen",
                        column: x => x.almIdAlmacen,
                        principalTable: "bmk_almacenes_alm",
                        principalColumn: "almIdAlmacen",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bmk_montacargas_mon",
                columns: table => new
                {
                    monIdMontacargas = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    almIdAlmacen = table.Column<int>(nullable: true),
                    monDescripcion = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_montacargas_mon", x => x.monIdMontacargas);
                    table.ForeignKey(
                        name: "FK_bmk_montacargas_mon_bmk_almacenes_alm_almIdAlmacen",
                        column: x => x.almIdAlmacen,
                        principalTable: "bmk_almacenes_alm",
                        principalColumn: "almIdAlmacen",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bmk_inventario_inv",
                columns: table => new
                {
                    invIdInventario = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    npIdNumeroParte = table.Column<int>(nullable: true),
                    invStock = table.Column<decimal>(nullable: false),
                    invStockReservado = table.Column<decimal>(nullable: false),
                    invOnOrder = table.Column<decimal>(nullable: false),
                    invMinimo = table.Column<int>(nullable: false),
                    invMaximo = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_inventario_inv", x => x.invIdInventario);
                    table.ForeignKey(
                        name: "FK_bmk_inventario_inv_bmk_numerosparte_np_npIdNumeroParte",
                        column: x => x.npIdNumeroParte,
                        principalTable: "bmk_numerosparte_np",
                        principalColumn: "npIdNumeroParte",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bmk_inventariolocacion_invl",
                columns: table => new
                {
                    IdInventarioLocacion = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InventarioinvIdInventario = table.Column<int>(nullable: false),
                    LocacionlocIdLocacion = table.Column<int>(nullable: false),
                    Cantidad = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_inventariolocacion_invl", x => x.IdInventarioLocacion);
                    table.ForeignKey(
                        name: "FK_bmk_inventariolocacion_invl_bmk_inventario_inv_InventarioinvIdInventario",
                        column: x => x.InventarioinvIdInventario,
                        principalTable: "bmk_inventario_inv",
                        principalColumn: "invIdInventario",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_bmk_inventariolocacion_invl_bmk_locaciones_loc_LocacionlocIdLocacion",
                        column: x => x.LocacionlocIdLocacion,
                        principalTable: "bmk_locaciones_loc",
                        principalColumn: "locIdLocacion",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_bmk_andenes_and_almIdAlmacen",
                table: "bmk_andenes_and",
                column: "almIdAlmacen");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_inventario_inv_npIdNumeroParte",
                table: "bmk_inventario_inv",
                column: "npIdNumeroParte");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_inventariolocacion_invl_InventarioinvIdInventario",
                table: "bmk_inventariolocacion_invl",
                column: "InventarioinvIdInventario");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_inventariolocacion_invl_LocacionlocIdLocacion",
                table: "bmk_inventariolocacion_invl",
                column: "LocacionlocIdLocacion");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_montacargas_mon_almIdAlmacen",
                table: "bmk_montacargas_mon",
                column: "almIdAlmacen");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "bmk_andenes_and");

            migrationBuilder.DropTable(
                name: "bmk_antenas_ant");

            migrationBuilder.DropTable(
                name: "bmk_inventariolocacion_invl");

            migrationBuilder.DropTable(
                name: "bmk_montacargas_mon");

            migrationBuilder.DropTable(
                name: "bmk_inventario_inv");

            migrationBuilder.DropTable(
                name: "bmk_locaciones_loc");

            migrationBuilder.DropTable(
                name: "bmk_almacenes_alm");

            migrationBuilder.DropTable(
                name: "bmk_numerosparte_np");
        }
    }
}
