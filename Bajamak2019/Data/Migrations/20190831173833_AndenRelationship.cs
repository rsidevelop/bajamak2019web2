﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bajamak2019.Migrations
{
    public partial class AndenRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "antIdAnden",
                table: "bmk_antenas_ant");

            migrationBuilder.AddColumn<int>(
                name: "andIdAnden",
                table: "bmk_antenas_ant",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_bmk_antenas_ant_andIdAnden",
                table: "bmk_antenas_ant",
                column: "andIdAnden");

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_antenas_ant_bmk_andenes_and_andIdAnden",
                table: "bmk_antenas_ant",
                column: "andIdAnden",
                principalTable: "bmk_andenes_and",
                principalColumn: "andIdAnden",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_bmk_antenas_ant_bmk_andenes_and_andIdAnden",
                table: "bmk_antenas_ant");

            migrationBuilder.DropIndex(
                name: "IX_bmk_antenas_ant_andIdAnden",
                table: "bmk_antenas_ant");

            migrationBuilder.DropColumn(
                name: "andIdAnden",
                table: "bmk_antenas_ant");

            migrationBuilder.AddColumn<int>(
                name: "antIdAnden",
                table: "bmk_antenas_ant",
                nullable: false,
                defaultValue: 0);
        }
    }
}
