﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bajamak2019.Migrations
{
    public partial class FixRelationsOrdersTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_bmk_andenes_and_bmk_almacenes_alm_almIdAlmacen",
                table: "bmk_andenes_and");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_antenas_ant_bmk_andenes_and_andIdAnden",
                table: "bmk_antenas_ant");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_inventario_inv_bmk_numerosparte_np_npIdNumeroParte",
                table: "bmk_inventario_inv");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_inventario_inv_InventarioinvIdInventario",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_locaciones_loc_LocacionlocIdLocacion",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_montacargas_mon_bmk_almacenes_alm_almIdAlmacen",
                table: "bmk_montacargas_mon");

            migrationBuilder.DropIndex(
                name: "IX_bmk_inventariolocacion_invl_InventarioinvIdInventario",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropIndex(
                name: "IX_bmk_inventariolocacion_invl_LocacionlocIdLocacion",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropColumn(
                name: "InventarioinvIdInventario",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropColumn(
                name: "LocacionlocIdLocacion",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.RenameColumn(
                name: "almIdAlmacen",
                table: "bmk_montacargas_mon",
                newName: "monIdAlmacen");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_montacargas_mon_almIdAlmacen",
                table: "bmk_montacargas_mon",
                newName: "IX_bmk_montacargas_mon_monIdAlmacen");

            migrationBuilder.RenameColumn(
                name: "Cantidad",
                table: "bmk_inventariolocacion_invl",
                newName: "invlCantidad");

            migrationBuilder.RenameColumn(
                name: "IdInventarioLocacion",
                table: "bmk_inventariolocacion_invl",
                newName: "invlIdInventarioLocacion");

            migrationBuilder.RenameColumn(
                name: "npIdNumeroParte",
                table: "bmk_inventario_inv",
                newName: "invIdNumeroParte");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_inventario_inv_npIdNumeroParte",
                table: "bmk_inventario_inv",
                newName: "IX_bmk_inventario_inv_invIdNumeroParte");

            migrationBuilder.RenameColumn(
                name: "andIdAnden",
                table: "bmk_antenas_ant",
                newName: "antIdAnden");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_antenas_ant_andIdAnden",
                table: "bmk_antenas_ant",
                newName: "IX_bmk_antenas_ant_antIdAnden");

            migrationBuilder.RenameColumn(
                name: "almIdAlmacen",
                table: "bmk_andenes_and",
                newName: "andIdAlmacen");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_andenes_and_almIdAlmacen",
                table: "bmk_andenes_and",
                newName: "IX_bmk_andenes_and_andIdAlmacen");

            migrationBuilder.AddColumn<int>(
                name: "invlIdInventario",
                table: "bmk_inventariolocacion_invl",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "invlIdLocacion",
                table: "bmk_inventariolocacion_invl",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_bmk_inventariolocacion_invl_invlIdInventario",
                table: "bmk_inventariolocacion_invl",
                column: "invlIdInventario");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_inventariolocacion_invl_invlIdLocacion",
                table: "bmk_inventariolocacion_invl",
                column: "invlIdLocacion");

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_andenes_and_bmk_almacenes_alm_andIdAlmacen",
                table: "bmk_andenes_and",
                column: "andIdAlmacen",
                principalTable: "bmk_almacenes_alm",
                principalColumn: "almIdAlmacen",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_antenas_ant_bmk_andenes_and_antIdAnden",
                table: "bmk_antenas_ant",
                column: "antIdAnden",
                principalTable: "bmk_andenes_and",
                principalColumn: "andIdAnden",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_inventario_inv_bmk_numerosparte_np_invIdNumeroParte",
                table: "bmk_inventario_inv",
                column: "invIdNumeroParte",
                principalTable: "bmk_numerosparte_np",
                principalColumn: "npIdNumeroParte",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_inventario_inv_invlIdInventario",
                table: "bmk_inventariolocacion_invl",
                column: "invlIdInventario",
                principalTable: "bmk_inventario_inv",
                principalColumn: "invIdInventario",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_locaciones_loc_invlIdLocacion",
                table: "bmk_inventariolocacion_invl",
                column: "invlIdLocacion",
                principalTable: "bmk_locaciones_loc",
                principalColumn: "locIdLocacion",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_montacargas_mon_bmk_almacenes_alm_monIdAlmacen",
                table: "bmk_montacargas_mon",
                column: "monIdAlmacen",
                principalTable: "bmk_almacenes_alm",
                principalColumn: "almIdAlmacen",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_bmk_andenes_and_bmk_almacenes_alm_andIdAlmacen",
                table: "bmk_andenes_and");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_antenas_ant_bmk_andenes_and_antIdAnden",
                table: "bmk_antenas_ant");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_inventario_inv_bmk_numerosparte_np_invIdNumeroParte",
                table: "bmk_inventario_inv");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_inventario_inv_invlIdInventario",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_locaciones_loc_invlIdLocacion",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_montacargas_mon_bmk_almacenes_alm_monIdAlmacen",
                table: "bmk_montacargas_mon");

            migrationBuilder.DropIndex(
                name: "IX_bmk_inventariolocacion_invl_invlIdInventario",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropIndex(
                name: "IX_bmk_inventariolocacion_invl_invlIdLocacion",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropColumn(
                name: "invlIdInventario",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropColumn(
                name: "invlIdLocacion",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.RenameColumn(
                name: "monIdAlmacen",
                table: "bmk_montacargas_mon",
                newName: "almIdAlmacen");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_montacargas_mon_monIdAlmacen",
                table: "bmk_montacargas_mon",
                newName: "IX_bmk_montacargas_mon_almIdAlmacen");

            migrationBuilder.RenameColumn(
                name: "invlCantidad",
                table: "bmk_inventariolocacion_invl",
                newName: "Cantidad");

            migrationBuilder.RenameColumn(
                name: "invlIdInventarioLocacion",
                table: "bmk_inventariolocacion_invl",
                newName: "IdInventarioLocacion");

            migrationBuilder.RenameColumn(
                name: "invIdNumeroParte",
                table: "bmk_inventario_inv",
                newName: "npIdNumeroParte");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_inventario_inv_invIdNumeroParte",
                table: "bmk_inventario_inv",
                newName: "IX_bmk_inventario_inv_npIdNumeroParte");

            migrationBuilder.RenameColumn(
                name: "antIdAnden",
                table: "bmk_antenas_ant",
                newName: "andIdAnden");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_antenas_ant_antIdAnden",
                table: "bmk_antenas_ant",
                newName: "IX_bmk_antenas_ant_andIdAnden");

            migrationBuilder.RenameColumn(
                name: "andIdAlmacen",
                table: "bmk_andenes_and",
                newName: "almIdAlmacen");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_andenes_and_andIdAlmacen",
                table: "bmk_andenes_and",
                newName: "IX_bmk_andenes_and_almIdAlmacen");

            migrationBuilder.AddColumn<int>(
                name: "InventarioinvIdInventario",
                table: "bmk_inventariolocacion_invl",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "LocacionlocIdLocacion",
                table: "bmk_inventariolocacion_invl",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_bmk_inventariolocacion_invl_InventarioinvIdInventario",
                table: "bmk_inventariolocacion_invl",
                column: "InventarioinvIdInventario");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_inventariolocacion_invl_LocacionlocIdLocacion",
                table: "bmk_inventariolocacion_invl",
                column: "LocacionlocIdLocacion");

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_andenes_and_bmk_almacenes_alm_almIdAlmacen",
                table: "bmk_andenes_and",
                column: "almIdAlmacen",
                principalTable: "bmk_almacenes_alm",
                principalColumn: "almIdAlmacen",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_antenas_ant_bmk_andenes_and_andIdAnden",
                table: "bmk_antenas_ant",
                column: "andIdAnden",
                principalTable: "bmk_andenes_and",
                principalColumn: "andIdAnden",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_inventario_inv_bmk_numerosparte_np_npIdNumeroParte",
                table: "bmk_inventario_inv",
                column: "npIdNumeroParte",
                principalTable: "bmk_numerosparte_np",
                principalColumn: "npIdNumeroParte",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_inventario_inv_InventarioinvIdInventario",
                table: "bmk_inventariolocacion_invl",
                column: "InventarioinvIdInventario",
                principalTable: "bmk_inventario_inv",
                principalColumn: "invIdInventario",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_locaciones_loc_LocacionlocIdLocacion",
                table: "bmk_inventariolocacion_invl",
                column: "LocacionlocIdLocacion",
                principalTable: "bmk_locaciones_loc",
                principalColumn: "locIdLocacion",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_montacargas_mon_bmk_almacenes_alm_almIdAlmacen",
                table: "bmk_montacargas_mon",
                column: "almIdAlmacen",
                principalTable: "bmk_almacenes_alm",
                principalColumn: "almIdAlmacen",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
