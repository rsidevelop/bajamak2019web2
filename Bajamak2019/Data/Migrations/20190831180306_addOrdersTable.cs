﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bajamak2019.Migrations
{
    public partial class addOrdersTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "andTipoAntena",
                table: "bmk_antenas_ant",
                newName: "antTipoAntena");

            migrationBuilder.CreateTable(
                name: "bmk_ordenes_ord",
                columns: table => new
                {
                    ordIdOrden = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ordNumeroParte = table.Column<int>(nullable: false),
                    ordCantidad = table.Column<decimal>(nullable: false),
                    ordTipoMovimiento = table.Column<string>(nullable: false),
                    ordIdAnden = table.Column<int>(nullable: false),
                    ordComentario = table.Column<string>(maxLength: 250, nullable: true),
                    ordFechaRegistro = table.Column<DateTime>(nullable: false),
                    ordFechaAtencion = table.Column<DateTime>(nullable: false),
                    ordIdMontacargas = table.Column<int>(nullable: true),
                    ordIdTag = table.Column<string>(maxLength: 50, nullable: true),
                    ordIdLocacion = table.Column<int>(nullable: false),
                    Status = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_ordenes_ord", x => x.ordIdOrden);
                    table.ForeignKey(
                        name: "FK_bmk_ordenes_ord_bmk_andenes_and_ordIdAnden",
                        column: x => x.ordIdAnden,
                        principalTable: "bmk_andenes_and",
                        principalColumn: "andIdAnden",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_bmk_ordenes_ord_bmk_locaciones_loc_ordIdLocacion",
                        column: x => x.ordIdLocacion,
                        principalTable: "bmk_locaciones_loc",
                        principalColumn: "locIdLocacion",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_bmk_ordenes_ord_bmk_montacargas_mon_ordIdMontacargas",
                        column: x => x.ordIdMontacargas,
                        principalTable: "bmk_montacargas_mon",
                        principalColumn: "monIdMontacargas",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_bmk_ordenes_ord_bmk_numerosparte_np_ordNumeroParte",
                        column: x => x.ordNumeroParte,
                        principalTable: "bmk_numerosparte_np",
                        principalColumn: "npIdNumeroParte",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_bmk_ordenes_ord_ordIdAnden",
                table: "bmk_ordenes_ord",
                column: "ordIdAnden");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_ordenes_ord_ordIdLocacion",
                table: "bmk_ordenes_ord",
                column: "ordIdLocacion");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_ordenes_ord_ordIdMontacargas",
                table: "bmk_ordenes_ord",
                column: "ordIdMontacargas");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_ordenes_ord_ordNumeroParte",
                table: "bmk_ordenes_ord",
                column: "ordNumeroParte");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "bmk_ordenes_ord");

            migrationBuilder.RenameColumn(
                name: "antTipoAntena",
                table: "bmk_antenas_ant",
                newName: "andTipoAntena");
        }
    }
}
