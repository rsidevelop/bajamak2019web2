﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bajamak2019.Migrations
{
    public partial class fixFieldNameOrdes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Status",
                table: "bmk_ordenes_ord",
                newName: "ordStatus");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ordStatus",
                table: "bmk_ordenes_ord",
                newName: "Status");
        }
    }
}
