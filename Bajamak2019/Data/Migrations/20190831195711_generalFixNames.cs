﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Bajamak2019.Migrations
{
    public partial class generalFixNames : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_bmk_inventario_inv_bmk_numerosparte_np_invIdNumeroParte",
                table: "bmk_inventario_inv");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_inventario_inv_invlIdInventario",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_locaciones_loc_invlIdLocacion",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropForeignKey(
                name: "FK_bmk_montacargas_mon_bmk_almacenes_alm_monIdAlmacen",
                table: "bmk_montacargas_mon");

            migrationBuilder.DropTable(
                name: "bmk_antenas_ant");

            migrationBuilder.DropTable(
                name: "bmk_ordenes_ord");

            migrationBuilder.DropTable(
                name: "bmk_andenes_and");

            migrationBuilder.DropTable(
                name: "bmk_locaciones_loc");

            migrationBuilder.DropTable(
                name: "bmk_numerosparte_np");

            migrationBuilder.DropTable(
                name: "bmk_almacenes_alm");

            migrationBuilder.DropPrimaryKey(
                name: "PK_bmk_montacargas_mon",
                table: "bmk_montacargas_mon");

            migrationBuilder.DropPrimaryKey(
                name: "PK_bmk_inventariolocacion_invl",
                table: "bmk_inventariolocacion_invl");

            migrationBuilder.DropPrimaryKey(
                name: "PK_bmk_inventario_inv",
                table: "bmk_inventario_inv");

            migrationBuilder.RenameTable(
                name: "bmk_montacargas_mon",
                newName: "Montacargas");

            migrationBuilder.RenameTable(
                name: "bmk_inventariolocacion_invl",
                newName: "InventarioLocacion");

            migrationBuilder.RenameTable(
                name: "bmk_inventario_inv",
                newName: "Inventario");

            migrationBuilder.RenameColumn(
                name: "monIdAlmacen",
                table: "Montacargas",
                newName: "IdAlmacen");

            migrationBuilder.RenameColumn(
                name: "monDescripcion",
                table: "Montacargas",
                newName: "Descripcion");

            migrationBuilder.RenameColumn(
                name: "monIdMontacargas",
                table: "Montacargas",
                newName: "IdMontacargas");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_montacargas_mon_monIdAlmacen",
                table: "Montacargas",
                newName: "IX_Montacargas_IdAlmacen");

            migrationBuilder.RenameColumn(
                name: "invlIdLocacion",
                table: "InventarioLocacion",
                newName: "IdLocacion");

            migrationBuilder.RenameColumn(
                name: "invlIdInventario",
                table: "InventarioLocacion",
                newName: "IdInventario");

            migrationBuilder.RenameColumn(
                name: "invlCantidad",
                table: "InventarioLocacion",
                newName: "Cantidad");

            migrationBuilder.RenameColumn(
                name: "invlIdInventarioLocacion",
                table: "InventarioLocacion",
                newName: "IdInventarioLocacion");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_inventariolocacion_invl_invlIdLocacion",
                table: "InventarioLocacion",
                newName: "IX_InventarioLocacion_IdLocacion");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_inventariolocacion_invl_invlIdInventario",
                table: "InventarioLocacion",
                newName: "IX_InventarioLocacion_IdInventario");

            migrationBuilder.RenameColumn(
                name: "invStockReservado",
                table: "Inventario",
                newName: "StockReservado");

            migrationBuilder.RenameColumn(
                name: "invStock",
                table: "Inventario",
                newName: "Stock");

            migrationBuilder.RenameColumn(
                name: "invOnOrder",
                table: "Inventario",
                newName: "OnOrder");

            migrationBuilder.RenameColumn(
                name: "invMinimo",
                table: "Inventario",
                newName: "Minimo");

            migrationBuilder.RenameColumn(
                name: "invMaximo",
                table: "Inventario",
                newName: "Maximo");

            migrationBuilder.RenameColumn(
                name: "invIdNumeroParte",
                table: "Inventario",
                newName: "IdNumeroParte");

            migrationBuilder.RenameColumn(
                name: "invIdInventario",
                table: "Inventario",
                newName: "IdInventario");

            migrationBuilder.RenameIndex(
                name: "IX_bmk_inventario_inv_invIdNumeroParte",
                table: "Inventario",
                newName: "IX_Inventario_IdNumeroParte");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Montacargas",
                table: "Montacargas",
                column: "IdMontacargas");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InventarioLocacion",
                table: "InventarioLocacion",
                column: "IdInventarioLocacion");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Inventario",
                table: "Inventario",
                column: "IdInventario");

            migrationBuilder.CreateTable(
                name: "Almacenes",
                columns: table => new
                {
                    IdAlmacen = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: false),
                    Activo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Almacenes", x => x.IdAlmacen);
                });

            migrationBuilder.CreateTable(
                name: "Locaciones",
                columns: table => new
                {
                    IdLocacion = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Descripcion = table.Column<string>(maxLength: 50, nullable: false),
                    Nivel = table.Column<string>(maxLength: 10, nullable: false),
                    Posicion = table.Column<int>(nullable: false),
                    NombreAntena = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locaciones", x => x.IdLocacion);
                });

            migrationBuilder.CreateTable(
                name: "NumerosParte",
                columns: table => new
                {
                    IdNumeroParte = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Codigo = table.Column<string>(maxLength: 50, nullable: false),
                    Descripcion = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NumerosParte", x => x.IdNumeroParte);
                });

            migrationBuilder.CreateTable(
                name: "Andenes",
                columns: table => new
                {
                    IdAnden = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdAlmacen = table.Column<int>(nullable: true),
                    TipoAnden = table.Column<string>(maxLength: 20, nullable: false),
                    Descripcion = table.Column<string>(maxLength: 150, nullable: false),
                    Activo = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Andenes", x => x.IdAnden);
                    table.ForeignKey(
                        name: "FK_Andenes_Almacenes_IdAlmacen",
                        column: x => x.IdAlmacen,
                        principalTable: "Almacenes",
                        principalColumn: "IdAlmacen",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Antenas",
                columns: table => new
                {
                    IdAntena = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Nombre = table.Column<string>(maxLength: 15, nullable: false),
                    IdAnden = table.Column<int>(nullable: true),
                    TipoAntena = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Antenas", x => x.IdAntena);
                    table.ForeignKey(
                        name: "FK_Antenas_Andenes_IdAnden",
                        column: x => x.IdAnden,
                        principalTable: "Andenes",
                        principalColumn: "IdAnden",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Ordenes",
                columns: table => new
                {
                    IdOrden = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    IdNumeroParte = table.Column<int>(nullable: false),
                    Cantidad = table.Column<decimal>(nullable: false),
                    TipoMovimiento = table.Column<string>(nullable: false),
                    IdAnden = table.Column<int>(nullable: false),
                    Comentario = table.Column<string>(maxLength: 250, nullable: true),
                    FechaRegistro = table.Column<DateTime>(nullable: false),
                    FechaAtencion = table.Column<DateTime>(nullable: false),
                    IdMontacargas = table.Column<int>(nullable: true),
                    IdTag = table.Column<string>(maxLength: 50, nullable: true),
                    IdLocacion = table.Column<int>(nullable: false),
                    Status = table.Column<string>(maxLength: 15, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Ordenes", x => x.IdOrden);
                    table.ForeignKey(
                        name: "FK_Ordenes_Andenes_IdAnden",
                        column: x => x.IdAnden,
                        principalTable: "Andenes",
                        principalColumn: "IdAnden",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Ordenes_Locaciones_IdLocacion",
                        column: x => x.IdLocacion,
                        principalTable: "Locaciones",
                        principalColumn: "IdLocacion",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Ordenes_Montacargas_IdMontacargas",
                        column: x => x.IdMontacargas,
                        principalTable: "Montacargas",
                        principalColumn: "IdMontacargas",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Ordenes_NumerosParte_IdNumeroParte",
                        column: x => x.IdNumeroParte,
                        principalTable: "NumerosParte",
                        principalColumn: "IdNumeroParte",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Andenes_IdAlmacen",
                table: "Andenes",
                column: "IdAlmacen");

            migrationBuilder.CreateIndex(
                name: "IX_Antenas_IdAnden",
                table: "Antenas",
                column: "IdAnden");

            migrationBuilder.CreateIndex(
                name: "IX_Ordenes_IdAnden",
                table: "Ordenes",
                column: "IdAnden");

            migrationBuilder.CreateIndex(
                name: "IX_Ordenes_IdLocacion",
                table: "Ordenes",
                column: "IdLocacion");

            migrationBuilder.CreateIndex(
                name: "IX_Ordenes_IdMontacargas",
                table: "Ordenes",
                column: "IdMontacargas");

            migrationBuilder.CreateIndex(
                name: "IX_Ordenes_IdNumeroParte",
                table: "Ordenes",
                column: "IdNumeroParte");

            migrationBuilder.AddForeignKey(
                name: "FK_Inventario_NumerosParte_IdNumeroParte",
                table: "Inventario",
                column: "IdNumeroParte",
                principalTable: "NumerosParte",
                principalColumn: "IdNumeroParte",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_InventarioLocacion_Inventario_IdInventario",
                table: "InventarioLocacion",
                column: "IdInventario",
                principalTable: "Inventario",
                principalColumn: "IdInventario",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_InventarioLocacion_Locaciones_IdLocacion",
                table: "InventarioLocacion",
                column: "IdLocacion",
                principalTable: "Locaciones",
                principalColumn: "IdLocacion",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Montacargas_Almacenes_IdAlmacen",
                table: "Montacargas",
                column: "IdAlmacen",
                principalTable: "Almacenes",
                principalColumn: "IdAlmacen",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Inventario_NumerosParte_IdNumeroParte",
                table: "Inventario");

            migrationBuilder.DropForeignKey(
                name: "FK_InventarioLocacion_Inventario_IdInventario",
                table: "InventarioLocacion");

            migrationBuilder.DropForeignKey(
                name: "FK_InventarioLocacion_Locaciones_IdLocacion",
                table: "InventarioLocacion");

            migrationBuilder.DropForeignKey(
                name: "FK_Montacargas_Almacenes_IdAlmacen",
                table: "Montacargas");

            migrationBuilder.DropTable(
                name: "Antenas");

            migrationBuilder.DropTable(
                name: "Ordenes");

            migrationBuilder.DropTable(
                name: "Andenes");

            migrationBuilder.DropTable(
                name: "Locaciones");

            migrationBuilder.DropTable(
                name: "NumerosParte");

            migrationBuilder.DropTable(
                name: "Almacenes");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Montacargas",
                table: "Montacargas");

            migrationBuilder.DropPrimaryKey(
                name: "PK_InventarioLocacion",
                table: "InventarioLocacion");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Inventario",
                table: "Inventario");

            migrationBuilder.RenameTable(
                name: "Montacargas",
                newName: "bmk_montacargas_mon");

            migrationBuilder.RenameTable(
                name: "InventarioLocacion",
                newName: "bmk_inventariolocacion_invl");

            migrationBuilder.RenameTable(
                name: "Inventario",
                newName: "bmk_inventario_inv");

            migrationBuilder.RenameColumn(
                name: "IdAlmacen",
                table: "bmk_montacargas_mon",
                newName: "monIdAlmacen");

            migrationBuilder.RenameColumn(
                name: "Descripcion",
                table: "bmk_montacargas_mon",
                newName: "monDescripcion");

            migrationBuilder.RenameColumn(
                name: "IdMontacargas",
                table: "bmk_montacargas_mon",
                newName: "monIdMontacargas");

            migrationBuilder.RenameIndex(
                name: "IX_Montacargas_IdAlmacen",
                table: "bmk_montacargas_mon",
                newName: "IX_bmk_montacargas_mon_monIdAlmacen");

            migrationBuilder.RenameColumn(
                name: "IdLocacion",
                table: "bmk_inventariolocacion_invl",
                newName: "invlIdLocacion");

            migrationBuilder.RenameColumn(
                name: "IdInventario",
                table: "bmk_inventariolocacion_invl",
                newName: "invlIdInventario");

            migrationBuilder.RenameColumn(
                name: "Cantidad",
                table: "bmk_inventariolocacion_invl",
                newName: "invlCantidad");

            migrationBuilder.RenameColumn(
                name: "IdInventarioLocacion",
                table: "bmk_inventariolocacion_invl",
                newName: "invlIdInventarioLocacion");

            migrationBuilder.RenameIndex(
                name: "IX_InventarioLocacion_IdLocacion",
                table: "bmk_inventariolocacion_invl",
                newName: "IX_bmk_inventariolocacion_invl_invlIdLocacion");

            migrationBuilder.RenameIndex(
                name: "IX_InventarioLocacion_IdInventario",
                table: "bmk_inventariolocacion_invl",
                newName: "IX_bmk_inventariolocacion_invl_invlIdInventario");

            migrationBuilder.RenameColumn(
                name: "StockReservado",
                table: "bmk_inventario_inv",
                newName: "invStockReservado");

            migrationBuilder.RenameColumn(
                name: "Stock",
                table: "bmk_inventario_inv",
                newName: "invStock");

            migrationBuilder.RenameColumn(
                name: "OnOrder",
                table: "bmk_inventario_inv",
                newName: "invOnOrder");

            migrationBuilder.RenameColumn(
                name: "Minimo",
                table: "bmk_inventario_inv",
                newName: "invMinimo");

            migrationBuilder.RenameColumn(
                name: "Maximo",
                table: "bmk_inventario_inv",
                newName: "invMaximo");

            migrationBuilder.RenameColumn(
                name: "IdNumeroParte",
                table: "bmk_inventario_inv",
                newName: "invIdNumeroParte");

            migrationBuilder.RenameColumn(
                name: "IdInventario",
                table: "bmk_inventario_inv",
                newName: "invIdInventario");

            migrationBuilder.RenameIndex(
                name: "IX_Inventario_IdNumeroParte",
                table: "bmk_inventario_inv",
                newName: "IX_bmk_inventario_inv_invIdNumeroParte");

            migrationBuilder.AddPrimaryKey(
                name: "PK_bmk_montacargas_mon",
                table: "bmk_montacargas_mon",
                column: "monIdMontacargas");

            migrationBuilder.AddPrimaryKey(
                name: "PK_bmk_inventariolocacion_invl",
                table: "bmk_inventariolocacion_invl",
                column: "invlIdInventarioLocacion");

            migrationBuilder.AddPrimaryKey(
                name: "PK_bmk_inventario_inv",
                table: "bmk_inventario_inv",
                column: "invIdInventario");

            migrationBuilder.CreateTable(
                name: "bmk_almacenes_alm",
                columns: table => new
                {
                    almIdAlmacen = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    almActivo = table.Column<bool>(nullable: false),
                    almDescripcion = table.Column<string>(maxLength: 150, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_almacenes_alm", x => x.almIdAlmacen);
                });

            migrationBuilder.CreateTable(
                name: "bmk_locaciones_loc",
                columns: table => new
                {
                    locIdLocacion = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    locDescripcion = table.Column<string>(maxLength: 50, nullable: false),
                    locNivel = table.Column<string>(maxLength: 10, nullable: false),
                    locNombreAntena = table.Column<string>(maxLength: 15, nullable: false),
                    locPosicion = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_locaciones_loc", x => x.locIdLocacion);
                });

            migrationBuilder.CreateTable(
                name: "bmk_numerosparte_np",
                columns: table => new
                {
                    npIdNumeroParte = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    npCodigo = table.Column<string>(maxLength: 50, nullable: false),
                    npDescripcion = table.Column<string>(maxLength: 250, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_numerosparte_np", x => x.npIdNumeroParte);
                });

            migrationBuilder.CreateTable(
                name: "bmk_andenes_and",
                columns: table => new
                {
                    andIdAnden = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    andActivo = table.Column<bool>(nullable: false),
                    andDescripcion = table.Column<string>(maxLength: 150, nullable: false),
                    andIdAlmacen = table.Column<int>(nullable: true),
                    andTipoAnden = table.Column<string>(maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_andenes_and", x => x.andIdAnden);
                    table.ForeignKey(
                        name: "FK_bmk_andenes_and_bmk_almacenes_alm_andIdAlmacen",
                        column: x => x.andIdAlmacen,
                        principalTable: "bmk_almacenes_alm",
                        principalColumn: "almIdAlmacen",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bmk_antenas_ant",
                columns: table => new
                {
                    antIdAntena = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    antIdAnden = table.Column<int>(nullable: true),
                    antNombre = table.Column<string>(maxLength: 15, nullable: false),
                    antTipoAntena = table.Column<string>(maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_antenas_ant", x => x.antIdAntena);
                    table.ForeignKey(
                        name: "FK_bmk_antenas_ant_bmk_andenes_and_antIdAnden",
                        column: x => x.antIdAnden,
                        principalTable: "bmk_andenes_and",
                        principalColumn: "andIdAnden",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "bmk_ordenes_ord",
                columns: table => new
                {
                    ordIdOrden = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ordCantidad = table.Column<decimal>(nullable: false),
                    ordComentario = table.Column<string>(maxLength: 250, nullable: true),
                    ordFechaAtencion = table.Column<DateTime>(nullable: false),
                    ordFechaRegistro = table.Column<DateTime>(nullable: false),
                    ordIdAnden = table.Column<int>(nullable: false),
                    ordIdLocacion = table.Column<int>(nullable: false),
                    ordIdMontacargas = table.Column<int>(nullable: true),
                    ordIdTag = table.Column<string>(maxLength: 50, nullable: true),
                    ordNumeroParte = table.Column<int>(nullable: false),
                    ordStatus = table.Column<string>(maxLength: 15, nullable: false),
                    ordTipoMovimiento = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_bmk_ordenes_ord", x => x.ordIdOrden);
                    table.ForeignKey(
                        name: "FK_bmk_ordenes_ord_bmk_andenes_and_ordIdAnden",
                        column: x => x.ordIdAnden,
                        principalTable: "bmk_andenes_and",
                        principalColumn: "andIdAnden",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_bmk_ordenes_ord_bmk_locaciones_loc_ordIdLocacion",
                        column: x => x.ordIdLocacion,
                        principalTable: "bmk_locaciones_loc",
                        principalColumn: "locIdLocacion",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_bmk_ordenes_ord_bmk_montacargas_mon_ordIdMontacargas",
                        column: x => x.ordIdMontacargas,
                        principalTable: "bmk_montacargas_mon",
                        principalColumn: "monIdMontacargas",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_bmk_ordenes_ord_bmk_numerosparte_np_ordNumeroParte",
                        column: x => x.ordNumeroParte,
                        principalTable: "bmk_numerosparte_np",
                        principalColumn: "npIdNumeroParte",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_bmk_andenes_and_andIdAlmacen",
                table: "bmk_andenes_and",
                column: "andIdAlmacen");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_antenas_ant_antIdAnden",
                table: "bmk_antenas_ant",
                column: "antIdAnden");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_ordenes_ord_ordIdAnden",
                table: "bmk_ordenes_ord",
                column: "ordIdAnden");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_ordenes_ord_ordIdLocacion",
                table: "bmk_ordenes_ord",
                column: "ordIdLocacion");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_ordenes_ord_ordIdMontacargas",
                table: "bmk_ordenes_ord",
                column: "ordIdMontacargas");

            migrationBuilder.CreateIndex(
                name: "IX_bmk_ordenes_ord_ordNumeroParte",
                table: "bmk_ordenes_ord",
                column: "ordNumeroParte");

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_inventario_inv_bmk_numerosparte_np_invIdNumeroParte",
                table: "bmk_inventario_inv",
                column: "invIdNumeroParte",
                principalTable: "bmk_numerosparte_np",
                principalColumn: "npIdNumeroParte",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_inventario_inv_invlIdInventario",
                table: "bmk_inventariolocacion_invl",
                column: "invlIdInventario",
                principalTable: "bmk_inventario_inv",
                principalColumn: "invIdInventario",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_inventariolocacion_invl_bmk_locaciones_loc_invlIdLocacion",
                table: "bmk_inventariolocacion_invl",
                column: "invlIdLocacion",
                principalTable: "bmk_locaciones_loc",
                principalColumn: "locIdLocacion",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_bmk_montacargas_mon_bmk_almacenes_alm_monIdAlmacen",
                table: "bmk_montacargas_mon",
                column: "monIdAlmacen",
                principalTable: "bmk_almacenes_alm",
                principalColumn: "almIdAlmacen",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
