﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Bajamak2019.Migrations
{
    public partial class AddFieldOnLocations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdAlmacen",
                table: "Locaciones",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Locaciones_IdAlmacen",
                table: "Locaciones",
                column: "IdAlmacen");

            migrationBuilder.AddForeignKey(
                name: "FK_Locaciones_Almacenes_IdAlmacen",
                table: "Locaciones",
                column: "IdAlmacen",
                principalTable: "Almacenes",
                principalColumn: "IdAlmacen",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Locaciones_Almacenes_IdAlmacen",
                table: "Locaciones");

            migrationBuilder.DropIndex(
                name: "IX_Locaciones_IdAlmacen",
                table: "Locaciones");

            migrationBuilder.DropColumn(
                name: "IdAlmacen",
                table: "Locaciones");
        }
    }
}
