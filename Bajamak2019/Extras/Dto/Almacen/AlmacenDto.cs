﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.Dto.Almacen
{
    public class AlmacenDto
    {
        public int IdAlmacen { get; set; }
        public string Descripcion { get; set; }
    }
}
