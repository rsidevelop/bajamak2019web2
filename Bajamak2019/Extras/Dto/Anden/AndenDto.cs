﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.Dto.Anden
{
    public class AndenDto
    {
        public int IdAnden { get; set; }
        public string Descripcion { get; set; }
    }
}
