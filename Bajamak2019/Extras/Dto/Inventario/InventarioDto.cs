﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Extras.Dto.NumeroParte;

namespace Bajamak2019.Extras.Dto.Inventario
{
    public class InventarioDto
    {
        public int IdInventario { get; set; }
        public NumeroParteDto NumeroParte { get; set; }
        public decimal Stock { get; set; }
        public decimal StockReservado { get; set; }
        public decimal OnOrder { get; set; }
        public int Minimo { get; set; }
        public int Maximo { get; set; }
    }
}
