﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Extras.Dto.Inventario;
using Bajamak2019.Extras.Dto.Locacion;

namespace Bajamak2019.Extras.Dto.InventarioLocacion
{
    public class InventarioLocacionDto
    {
        public int IdInventarioLocacion { get; set; }
        public decimal Cantidad { get; set; }
        public InventarioDto Inventario { get; set; }
        public LocacionDto Locacion { get; set; }
    }
}
