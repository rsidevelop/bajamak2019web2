﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Extras.Dto.Almacen;

namespace Bajamak2019.Extras.Dto.Locacion
{
    public class LocacionDto
    {
        public int IdLocacion { get; set; }
        public string Descripcion { get; set; }
        public string Nivel { get; set; }
        public int Posicion { get; set; }
        public string NombreAntena { get; set; }
        public AlmacenDto Almacen { get; set; }
    }
}
