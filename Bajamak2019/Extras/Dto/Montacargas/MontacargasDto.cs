﻿using Bajamak2019.Extras.Dto.Almacen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.Dto.Montacargas
{
    public class MontacargasDto
    {
        public int IdMontacargas { get; set; }
        public AlmacenDto Almacen { get; set; }
        public string Descripcion { get; set; }
    }

}
