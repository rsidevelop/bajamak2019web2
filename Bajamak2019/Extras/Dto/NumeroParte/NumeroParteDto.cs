﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.Dto.NumeroParte
{
    public class NumeroParteDto
    {
        public int IdNumeroParte { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
    }
}
