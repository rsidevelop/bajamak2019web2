﻿using Bajamak2019.Extras.Dto.Anden;
using Bajamak2019.Extras.Dto.Locacion;
using Bajamak2019.Extras.Dto.Montacargas;
using Bajamak2019.Extras.Dto.NumeroParte;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.Dto.Orden
{
    public class OrdenDto
    {
        public int IdOrden { get; set; }
        public decimal Cantidad { get; set; }
        public string TipoMovimiento { get; set; }
        public string Comentario { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaAtencion { get; set; }

        public string FechaRegistroFormateada { get; set; }
        public string FechaAtencionFormateada { get; set; }
        public string IdTag { get; set; }
        public NumeroParteDto NumeroParte { get; set; }
        public MontacargasDto Montacargas { get; set; }
        public AndenDto Anden { get; set; }
        public LocacionDto Locacion { get; set; }
        public int IdNumeroParte { get; set; }
        public int IdMontacargas { get; set; }
        public int IdAnden { get; set;}
        public int IdLocacion { get; set; }
        public string Status { get; set; }

    }
}
