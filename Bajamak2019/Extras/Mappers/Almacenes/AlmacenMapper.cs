﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Extras.Dto.Almacen;
using Bajamak2019.Models;

namespace Bajamak2019.Extras.Mappers.Almacenes
{
    public static class AlmacenMapper
    {
        public static AlmacenDto ToAlmacenDto(this Almacen a)
        {
            return new AlmacenDto
            {
                IdAlmacen = a.IdAlmacen,
                Descripcion = a.Descripcion
            };
        }

        public static List<AlmacenDto> ToAlmacenListDto(this IEnumerable<Almacen> l)
        {
            return l.Select(a => a.ToAlmacenDto()).ToList();
        }
    }
}
