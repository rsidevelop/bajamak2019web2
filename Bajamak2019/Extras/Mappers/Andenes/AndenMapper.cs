﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Extras.Dto.Anden;
using Bajamak2019.Models;

namespace Bajamak2019.Extras.Mappers.Andenes
{
    public static class AndenMapper
    {
        public static AndenDto ToAndenDto(this Anden a)
        {
            return new AndenDto
            {
                IdAnden = a.IdAnden,
                Descripcion = a.Descripcion,
            };
        }

        public static List<AndenDto> ToAndenListDto(this IEnumerable<Anden> l)
        {
            return l.Select(a => a.ToAndenDto()).ToList();
        }
    }
}
