﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Extras.Dto.Inventario;
using Bajamak2019.Extras.Mappers.NumerosParte;
using Bajamak2019.Models;

namespace Bajamak2019.Extras.Mappers
{
    public static class InventarioMapper
    {
        public static InventarioDto ToInventarioDto(this Inventario i)
        {
            return new InventarioDto
            {
                IdInventario = i.IdInventario,
                NumeroParte = i.NumeroParte.ToNumeroParteDto(),
                Stock = i.Stock,
                StockReservado = i.StockReservado,
                OnOrder = i.OnOrder,
                Minimo = i.Minimo,
                Maximo = i.Maximo
            };
        }

        public static List<InventarioDto> ToInventarioListDto(this IEnumerable<Inventario> l)
        {
            return l.Select(i => i.ToInventarioDto()).ToList();
        }
    }
}
