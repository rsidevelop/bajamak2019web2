﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Extras.Dto.InventarioLocacion;
using Bajamak2019.Extras.Mappers.Locaciones;
using Bajamak2019.Models;

namespace Bajamak2019.Extras.Mappers.LocacionInventario
{
    public static class InventarioLocacionMapper
    {
        public static InventarioLocacionDto ToInventarioLocacionDto(this InventarioLocacion il)
        {
            return new InventarioLocacionDto
            {
                IdInventarioLocacion = il.IdInventarioLocacion,
                Cantidad = il.Cantidad,
                Inventario = il.Inventario.ToInventarioDto(),
                Locacion = il.Locacion.ToLocacionDto()
            };
        }

        public static List<InventarioLocacionDto> ToInventarioLocacionListDto(this IEnumerable<InventarioLocacion> l)
        {
            return l.Select(il => il.ToInventarioLocacionDto()).ToList();
        }
    }
}
