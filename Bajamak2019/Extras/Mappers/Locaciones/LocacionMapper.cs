﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Data;
using Bajamak2019.Extras.Dto.Locacion;
using Bajamak2019.Extras.Mappers.Almacenes;
using Bajamak2019.Models;

namespace Bajamak2019.Extras.Mappers.Locaciones
{
    public static class LocacionMapper
    {
        public static LocacionDto ToLocacionDto(this Locacion l)
        {
            return new LocacionDto
            {
                IdLocacion = l.IdLocacion,
                Descripcion = l.Descripcion,
                Nivel = l.Nivel,
                Posicion = l.Posicion,
                NombreAntena = l.NombreAntena,
                Almacen = l.Almacen.ToAlmacenDto()
            };
        }

        public static List<LocacionDto> ToLocacionListDto(this IEnumerable<Locacion> l)
        {
            return l.Select(lo => lo.ToLocacionDto()).ToList();
        }
    }
}
