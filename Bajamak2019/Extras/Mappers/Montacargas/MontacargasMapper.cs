﻿using Bajamak2019.Extras.Dto.Montacargas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Models;
using Bajamak2019.Extras.Mappers.Almacenes;

namespace Bajamak2019.Extras.Mappers.Montacargas
{
    public static class MontacargasMapper
    {
        public static MontacargasDto ToMontacargasDto(this Bajamak2019.Models.Montacargas mc)
        {
            return new MontacargasDto
            {
                IdMontacargas = mc.IdMontacargas,
                Descripcion = mc.Descripcion,
                Almacen = mc.Almacen.ToAlmacenDto(),
            };
        }
        public static List<MontacargasDto> ToMontacargasListDto(this IEnumerable<Bajamak2019.Models.Montacargas> l)
        {
            return l.Select(m => m.ToMontacargasDto()).ToList();
        }
    }
}
