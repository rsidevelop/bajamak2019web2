﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Data;
using Bajamak2019.Extras.Dto.NumeroParte;
using Bajamak2019.Extras.Services;
using Bajamak2019.Models;

namespace Bajamak2019.Extras.Mappers.NumerosParte
{
    public static class NumeroParteMapper
    {
        public static NumeroParteDto ToNumeroParteDto(this NumeroParte np)
        {
           // NumeroParteService service = new NumeroParteService();

            return new NumeroParteDto
            {
                IdNumeroParte = np.IdNumeroParte,
                Codigo = np.Codigo,
                Descripcion = np.Descripcion
            };
        }

        public static List<NumeroParteDto> ToNumeroParteListDto(this IEnumerable<NumeroParte> l)
        {
            return l.Select(np => np.ToNumeroParteDto()).ToList();
        }
    }
}
