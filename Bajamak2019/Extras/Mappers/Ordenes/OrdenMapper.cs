﻿using Bajamak2019.Extras.Dto.NumeroParte;
using Bajamak2019.Extras.Dto.Orden;
using Bajamak2019.Extras.Mappers.Andenes;
using Bajamak2019.Extras.Mappers.Locaciones;
using Bajamak2019.Extras.Mappers.Montacargas;
using Bajamak2019.Extras.Mappers.NumerosParte;
using Bajamak2019.Extras.Messages;
using Bajamak2019.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.Mappers.Ordenes
{
    public static class OrdenMapper
    {
        public static OrdenDto ToOrdenDto(this Orden or)
        {
        //    var numeroParte = _contexto.NumerosParte.FirstOrDefault(np => np.IdNumeroParte == or.IdNumeroParte).ToNumeroParteDto();
        //    var locacion = _contexto.Locaciones.FirstOrDefault(l => l.IdLocacion == or.IdLocacion).ToLocacionDto();
        //    var anden = _contexto.Andenes.FirstOrDefault(a => a.IdAnden == or.IdAnden).ToAndenDto();
        //    var montacargas = _contexto.Montacargas.FirstOrDefault(m => m.IdMontacargas == or.IdMontacargas).ToMontacargasDto();

            return new OrdenDto
            {
                Cantidad = or.Cantidad,
                Comentario = or.Comentario,
                FechaRegistro = DateTime.Today,
                FechaRegistroFormateada = DateTime.Today.ToString("dd/MM, HH:mm:ss tt"),
                Status = or.Status,
                IdTag = or.IdTag,
                TipoMovimiento = or.TipoMovimiento,
                //NumeroParte = numeroParte,
                //Locacion = locacion,
                //Anden = anden,
                //Montacargas = montacargas,
                NumeroParte = or.NumeroParte.ToNumeroParteDto(),  //NONE
                Locacion = or.Locacion.ToLocacionDto(),  //IDALMACEN FORANEA
                Anden = or.Anden.ToAndenDto(),  //IDALMACEN FORANEA
                Montacargas = or.Montacargas.ToMontacargasDto(),    //IDALMACEN FORANEA
                FechaAtencion = or.FechaAtencion,
                FechaAtencionFormateada = or.FechaAtencion.ToString("dd/MM, HH:mm:ss tt"),
                IdOrden = or.IdOrden,
            };
        }
        public static Orden ToOrdenModelo(this OrdenDto or)
        {

            return new Orden
            {
                Cantidad = or.Cantidad,
                Comentario = or.Comentario,
                FechaRegistro = DateTime.Today,
                Status = or.Status,
                IdTag = or.IdTag,
                TipoMovimiento = or.TipoMovimiento,
                IdNumeroParte = or.IdNumeroParte,
                IdLocacion = or.IdLocacion,
                IdAnden = or.IdAnden,
                IdMontacargas = or.IdMontacargas, 
                FechaAtencion = or.FechaAtencion,
                IdOrden = or.IdOrden,
            };
        }
        public static Orden ToOrdenModelo(this NuevaOrdenRequest or)
        {

            return new Orden
            {
                Cantidad = or.Cantidad,
                Comentario = or.Comentario,
                FechaRegistro = DateTime.Today,
                Status = or.Status,
                IdTag = or.IdTag,
                TipoMovimiento = or.TipoMovimiento,
                IdNumeroParte = or.IdNumeroParte,
                IdLocacion = or.IdLocacion,
                IdAnden = or.IdAnden,
                IdMontacargas = or.IdMontacargas,
                FechaAtencion = or.FechaAtencion,
                IdOrden = or.IdOrden,
            };
        }
        public static List<OrdenDto> ToOrdenListDto(this IEnumerable<Orden> list)
        {
            return list.Select(or => or.ToOrdenDto()).ToList();
        }
    }
    
}
