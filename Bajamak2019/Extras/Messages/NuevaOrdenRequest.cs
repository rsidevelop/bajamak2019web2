﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.Messages
{
    public class NuevaOrdenRequest
    {
        public int IdOrden { get; set; }
        public decimal Cantidad { get; set; }
        public string TipoMovimiento { get; set; }
        public string Comentario { get; set; }
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaAtencion { get; set; }
        public string IdTag { get; set; }
        public int IdNumeroParte { get; set; }
        public int IdMontacargas { get; set; }
        public int IdAnden { get; set; }
        public int IdLocacion { get; set; }
        public string Status { get; set; }
        public int IdAlmacen { get; set; }
    }
}
