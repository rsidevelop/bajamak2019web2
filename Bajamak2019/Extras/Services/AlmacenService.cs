﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Data;
using Bajamak2019.Extras.Dto.Almacen;
using Bajamak2019.Extras.Mappers.Almacenes;

namespace Bajamak2019.Extras.Services
{
    public interface IAlmacen
    {
        IEnumerable<AlmacenDto> TraeListaAlmacenes();
    }

    public class AlmacenService : IAlmacen
    {
        private readonly ApplicationDbContext _context;

        public AlmacenService(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<AlmacenDto> TraeListaAlmacenes()
        {
            return _context.Almacenes.ToAlmacenListDto();
        }
    }
}
