﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Data;
using Bajamak2019.Extras.Mappers.Andenes;
using Bajamak2019.Extras.Dto.Anden;
using Bajamak2019.Extras.ViewModels.Anden;

namespace Bajamak2019.Extras.Services
{
    public interface IAnden
    {
        IEnumerable<AndenDto> TraeListaAndenes(AndenViewModel request);
        IEnumerable<AndenDto> TraeListaTodosAndenes();
    }

    public class AndenService : IAnden
    {
        private readonly ApplicationDbContext _context;

        public AndenService(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<AndenDto> TraeListaAndenes(AndenViewModel request)
        {
            return _context.Andenes.Where(a => a.Almacen.IdAlmacen == request.IdAlmacen && a.TipoAnden == request.TipoAnden).ToAndenListDto();
        }
        public IEnumerable<AndenDto> TraeListaTodosAndenes()
        {
            return _context.Andenes.ToAndenListDto();
        }
    }
}
