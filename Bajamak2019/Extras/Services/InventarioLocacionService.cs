﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Data;
using Bajamak2019.Extras.Dto.InventarioLocacion;
using Bajamak2019.Extras.Dto.Locacion;
using Bajamak2019.Extras.Mappers.Almacenes;
using Bajamak2019.Extras.Mappers.Locaciones;
using Bajamak2019.Extras.Mappers.LocacionInventario;
using Bajamak2019.Extras.ViewModels.InventarioLocacion;
using Microsoft.EntityFrameworkCore;

namespace Bajamak2019.Extras.Services
{
    public interface IInventarioLocacion
    {
        IEnumerable<InventarioLocacionDto> TraeListaLocacionesInventario(InventarioLocacionViewModel request);
        IEnumerable<LocacionDto> TraeListaLocacionesDisponibles(int idAlmacen);
        IEnumerable<LocacionDto> TraeListaLocacionesPorParte(int idAlmacen, int idNumeroParte, int Cantidad);
    }

    public class InventarioLocacionService : IInventarioLocacion
    {
        private readonly ApplicationDbContext _context;

        public InventarioLocacionService(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<InventarioLocacionDto> TraeListaLocacionesInventario(InventarioLocacionViewModel request)
        {
            return _context.InventarioLocacion.Where(il => il.Inventario.NumeroParte.IdNumeroParte == request.IdNumeroParte && il.Locacion.Almacen.IdAlmacen == request.IdAlmacen && il.Cantidad != 0).Include(il => il.Locacion).ThenInclude(l => l.Almacen).ToInventarioLocacionListDto();
        }

        public IEnumerable<LocacionDto> TraeListaLocacionesDisponibles(int idAlmacen)
        {
            var locacionesTodas = _context.Locaciones.Where(l => l.Almacen.IdAlmacen == idAlmacen).Include(l => l.Almacen).ToLocacionListDto();
            var locacionesDisponibles = new List<LocacionDto>();


            foreach (var locacion in locacionesTodas)
            {
                var inventarioLocacion = _context.InventarioLocacion.Where(il => il.Locacion.IdLocacion == locacion.IdLocacion);

                if (!inventarioLocacion.Any() || inventarioLocacion.All(il => il.Cantidad == 0))
                {
                    locacionesDisponibles.Add(locacion);
                }
            }

            return locacionesDisponibles;
        }
        public IEnumerable<LocacionDto> TraeListaLocacionesPorParte(int idAlmacen, int idNumeroParte, int Cantidad)
        {
            //RESULTADO DESEADO: TODAS LAS LOCACIONES DONDE ESTE ESTE NUMERO DE PARTE EN ESTE ALMACEN
            var locacionesTodas = _context.Locaciones.Where(l => l.Almacen.IdAlmacen == idAlmacen).Include(l => l.Almacen).ToLocacionListDto();
            var locacionesPorParte = new List<LocacionDto>();


            //var innerJoin = from l in _context.Locaciones
            //                join il in _context.InventarioLocacion on l.IdLocacion equals il.Locacion.IdLocacion && 
            //                select new { l };


            var query= from l in _context.Locaciones
                             join il in _context.InventarioLocacion on l.IdLocacion equals il.Locacion.IdLocacion
                            join i in _context.Inventario on il.Inventario.IdInventario equals i.IdInventario
                            where l.Almacen.IdAlmacen == idAlmacen && il.Cantidad > Cantidad && i.NumeroParte.IdNumeroParte == idNumeroParte
                            select new LocacionDto{
                                IdLocacion = l.IdLocacion,
                                Descripcion = l.Descripcion,
                                Nivel = l.Nivel,
                                Posicion = l.Posicion,
                                NombreAntena = l.NombreAntena,
                                Almacen = l.Almacen.ToAlmacenDto()
                            };


            //select Locaciones.IdLocacion from Locaciones, InventarioLocacion, Inventario where
            //                            Locaciones.IdAlmacen = 1 and
            //                            InventarioLocacion.IdLocacion = Locaciones.IdLocacion and
            //                            InventarioLocacion.IdInventario = Inventario.IdInventario and
            //                            Inventario.IdNumeroParte = 1 and
            //                            InventarioLocacion.Cantidad > 0



            return query;
            
        }
    }
}
