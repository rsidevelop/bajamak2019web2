﻿using Bajamak2019.Data;
using Bajamak2019.Extras.Dto.Inventario;
using Bajamak2019.Extras.Mappers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.Services
{
    public interface IInventario
    {
        IEnumerable<InventarioDto> TraeListaInventario();
    }
    public class InventarioService : IInventario
    {
        private readonly ApplicationDbContext _context;

        public InventarioService(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<InventarioDto> TraeListaInventario()
        {
            return _context.Inventario.Include(or => or.NumeroParte).ToInventarioListDto();
        }
    }
}