﻿using Bajamak2019.Data;
using Bajamak2019.Extras.Dto.Montacargas;
using Bajamak2019.Extras.Mappers.Montacargas;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.Services
{
    public interface IMontacargas
    {
        int TraeMontacargasAsignado(int idAlmacen);
    }
    public class MontacargasService : IMontacargas
    {
        private readonly ApplicationDbContext _context;

        public MontacargasService(ApplicationDbContext context)
        {
            _context = context;
        }
        public int TraeMontacargasAsignado(int idAlmacen)
        {
            //Traer todos los Montacargas asociados a tal almacen
            //foreach (montacargas)
            //Calcular número de ordenes pendientes de ese montacargas
            //Guardar como en un array temporal
            //Obtener el montacargas con el minimo de ordenes pendientes 

            //Traer todos los Montacargas asociados a tal almacen
            var montas = _context.Montacargas.Where(monta => monta.Almacen.IdAlmacen == idAlmacen).Include(monta => monta.Almacen).ToMontacargasListDto();
            var montacargasColaList = new List<MontacargasCola>();
            //foreach (montacargas)
            foreach (var monta in montas)
            {
                //Calcular número de ordenes pendientes de ese montacargas
                var totalCola = (from ordenes in _context.Ordenes where ordenes.Status !="ENTREGADO" && ordenes.Status !="ALMACENADO" && ordenes.IdMontacargas == monta.IdMontacargas select ordenes.Status).Count();
                //Guardar como en un array temporal
                montacargasColaList.Add(new MontacargasCola(totalCola,monta.IdMontacargas));
            }
            int min = int.MaxValue;
            int idMinMonta=0;
            foreach (MontacargasCola monta in montacargasColaList)
            {
                if (monta.ordenesEnCola < min)
                {
                    min = monta.ordenesEnCola;
                    idMinMonta = monta.idMontacargas;
                }
            }

            //Obtener el montacargas con el minimo de ordenes pendientes 


            return idMinMonta;
        }
        public class MontacargasCola
        {
            public int ordenesEnCola;
            public int idMontacargas;
            public MontacargasCola(int ordenesCola, int idMonta)
            {
                ordenesEnCola = ordenesCola;
                idMontacargas = idMonta;
            }
        }
    }
}

