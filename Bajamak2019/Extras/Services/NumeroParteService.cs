﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bajamak2019.Data;
using Bajamak2019.Extras.Dto.NumeroParte;
using Bajamak2019.Extras.Mappers.NumerosParte;
using Bajamak2019.Models;

namespace Bajamak2019.Extras.Services
{
    public interface INumeroParte
    {
        IEnumerable<NumeroParteDto> TraeListaNumerosParte();
    }

    public class NumeroParteService : INumeroParte
    {
        private readonly ApplicationDbContext _context;

        public NumeroParteService(ApplicationDbContext context)
        {
            _context = context;
        }

        public IEnumerable<NumeroParteDto> TraeListaNumerosParte()
        {
            return _context.NumerosParte.ToNumeroParteListDto();
        }
        //public NumeroParte TraerNumeroPartePorId(int id)
        //{
        //    return _context.NumerosParte.FirstOrDefault(np => np.IdNumeroParte == id);
        //}
    }
}
