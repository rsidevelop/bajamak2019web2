﻿using Bajamak2019.Data;
using Bajamak2019.Extras.Dto.Montacargas;
using Bajamak2019.Extras.Dto.Orden;
using Bajamak2019.Extras.Mappers.Montacargas;
using Bajamak2019.Extras.Mappers.Ordenes;
using Bajamak2019.Extras.Messages;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.Services
{

    public interface IOrden
    {
        IEnumerable<OrdenDto> TraeListaOrdenes();
        IEnumerable<OrdenDto> TraeListaOrdenes(int idAlmacen);
        string GuardarOrden(NuevaOrdenRequest orden);

    }
    public class OrdenService : IOrden
    {
        private readonly ApplicationDbContext _context;

        public OrdenService(ApplicationDbContext context)
        {
            _context = context;
        }

        public string GuardarOrden(NuevaOrdenRequest ordenRequest)
        {
            String response = "";
            try
            {

            ordenRequest.IdMontacargas = TraeMontacargasAsignado(ordenRequest.IdAlmacen);
            Models.Orden ordenModelo = ordenRequest.ToOrdenModelo();
            _context.Ordenes.Add(ordenModelo);
            _context.SaveChanges();

                response = "Orden Agregada Exitosamente";

            }
            catch (Exception ex)
            {
                response = "Error: " + ex;
            }
            return response;
        }

        public IEnumerable<OrdenDto> TraeListaOrdenes()
        {
            var ordenes = _context.Ordenes.Include(or => or.NumeroParte).Include(or => or.Locacion).Include(or => or.Locacion.Almacen).Include(or => or.Anden).Include(or => or.Anden.Almacen).Include(or => or.Montacargas).Include(or => or.Montacargas.Almacen).ToOrdenListDto();
            
            return ordenes;
        }

        public IEnumerable<OrdenDto> TraeListaOrdenes(int idAlmacen)
        {
            var ordenes = _context.Ordenes.Where(orden => orden.Montacargas.Almacen.IdAlmacen == idAlmacen).Include(or => or.NumeroParte).Include(or => or.Locacion).Include(or => or.Locacion.Almacen).Include(or => or.Anden).Include(or => or.Anden.Almacen).Include(or => or.Montacargas).Include(or => or.Montacargas.Almacen).ToOrdenListDto();

            return ordenes;
        }
        public int TraeMontacargasAsignado(int idAlmacen)
        {
            List<MontacargasDto> montas = _context.Montacargas.Where(monta => monta.Almacen.IdAlmacen == idAlmacen).Include(monta => monta.Almacen).ToMontacargasListDto();
            var montacargasColaList = new List<MontacargasCola>();
            //foreach (montacargas)
            foreach (var monta in montas)
            {
                //Calcular número de ordenes pendientes de ese montacargas
                var totalCola = (from ordenes in _context.Ordenes where ordenes.Status != "ENTREGADO" && ordenes.Status != "ALMACENADO" && ordenes.IdMontacargas == monta.IdMontacargas select ordenes.Status).Count();
                //Guardar como en un array temporal
                montacargasColaList.Add(new MontacargasCola(totalCola, monta.IdMontacargas));
            }
            int min = int.MaxValue;
            int idMinMonta = 0;
            foreach (MontacargasCola monta in montacargasColaList)
            {
                if (monta.ordenesEnCola < min)
                {
                    min = monta.ordenesEnCola;
                    idMinMonta = monta.idMontacargas;
                }
            }

            //Obtener el montacargas con el minimo de ordenes pendientes 


            return idMinMonta;
        }
        public class MontacargasCola
        {
            public int ordenesEnCola;
            public int idMontacargas;
            public MontacargasCola(int ordenesCola, int idMonta)
            {
                ordenesEnCola = ordenesCola;
                idMontacargas = idMonta;
            }
        }
    }
    
}
