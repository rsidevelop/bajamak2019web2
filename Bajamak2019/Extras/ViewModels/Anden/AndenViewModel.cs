﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.ViewModels.Anden
{
    public class AndenViewModel
    {
        [Required]
        public int IdAlmacen { get; set; }
        [Required]
        public string TipoAnden { get; set; }
    }
}
