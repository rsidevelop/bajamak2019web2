﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Extras.ViewModels.InventarioLocacion
{
    public class InventarioLocacionViewModel
    {
        [Required]
        public int IdAlmacen { get; set; }
        public int IdNumeroParte { get; set; }
    }
}
