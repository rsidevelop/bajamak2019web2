﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Models
{
    public class Almacen
    {
        [Key]
        public int IdAlmacen { get; set; }
        [Required, StringLength(150)]
        public string Descripcion { get; set; }
        [Required]
        public bool Activo { get; set; }
    }
}
