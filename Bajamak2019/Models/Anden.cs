﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Models
{
    public class Anden
    {
        [Key]
        public int IdAnden { get; set; }
        [ForeignKey("IdAlmacen")]
        public Almacen Almacen { get; set; }
        [Required, StringLength(20)]
        public string TipoAnden { get; set; }
        [Required, StringLength(150)]
        public string Descripcion { get; set; }
        [Required]
        public bool Activo { get; set; }
    }
}
