﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Models
{
    public class Antena
    {
        [Key]
        public int IdAntena { get; set; }
        [Required, StringLength(15)]
        public string Nombre { get; set; }
        [ForeignKey("IdAnden")]
        public Anden Anden { get; set; }
        [Required, StringLength(10)]
        public string TipoAntena { get; set; }
    }
}
