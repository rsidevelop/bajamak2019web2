﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Models
{
    public class Inventario
    {
        [Key]
        public int IdInventario { get; set; }
        [ForeignKey("IdNumeroParte")]
        public NumeroParte NumeroParte { get; set; }
        public decimal Stock { get; set; }
        public decimal StockReservado { get; set; }
        public decimal OnOrder { get; set; }
        public int Minimo { get; set; }
        public int Maximo { get; set; }
    }
}
