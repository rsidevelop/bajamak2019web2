﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Models
{
    public class InventarioLocacion
    {
        [Key]
        public int IdInventarioLocacion { get; set; }
        [Required, ForeignKey("IdInventario")]
        public Inventario Inventario { get; set; }
        [Required, ForeignKey("IdLocacion")]
        public Locacion Locacion { get; set; }
        [Required]
        public decimal Cantidad { get; set; }
    }
}
