﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Models
{
    public class Locacion
    {
        [Key]
        public int IdLocacion { get; set; }
        [ForeignKey("IdAlmacen")]
        public Almacen Almacen { get; set; }
        [Required, StringLength(50)]
        public string Descripcion { get; set; }
        [Required, StringLength(10)]
        public string Nivel { get; set; }
        [Required]
        public int Posicion { get; set; }
        [Required, StringLength(15)]
        public string NombreAntena { get; set; }
    }
}
