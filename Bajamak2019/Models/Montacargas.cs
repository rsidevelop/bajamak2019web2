﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Models
{
    public class Montacargas
    {
        [Key]
        public int IdMontacargas { get; set; }
        [ForeignKey("IdAlmacen")]
        public Almacen Almacen { get; set; }
        [Required, StringLength(50)]
        public string Descripcion { get; set; }
    }
}
