﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Bajamak2019.Models
{
    public class NumeroParte
    {
        [Key]
        public int IdNumeroParte { get; set; }
        [Required, StringLength(50)]
        public string Codigo { get; set; }
        [Required, StringLength(250)]
        public string Descripcion { get; set; }
    }
}
