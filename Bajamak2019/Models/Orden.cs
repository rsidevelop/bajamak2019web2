﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace Bajamak2019.Models
{
    public class Orden
    {
        [Key]
        public int IdOrden { get; set; }
        [Required, ForeignKey("IdNumeroParte")]
        public NumeroParte NumeroParte { get; set; }
        public int IdNumeroParte { get; set; }
        [Required]
        public decimal Cantidad { get; set; }
        [Required]
        public string TipoMovimiento { get; set; }
        [Required, ForeignKey("IdAnden")]
        public Anden Anden { get; set; }
        public int IdAnden { get; set; }
        [StringLength(250)]
        public string Comentario { get; set; }
        [Required]
        public DateTime FechaRegistro { get; set; }
        public DateTime FechaAtencion { get; set; }
        [ForeignKey("IdMontacargas")]
        public Montacargas Montacargas { get; set; }
        public int IdMontacargas { get; set; }
        [StringLength(50)]
        public string IdTag { get; set; }
        [Required, ForeignKey("IdLocacion")]
        public Locacion Locacion { get; set; }
        public int IdLocacion { get; set; }
        [Required, StringLength(15)]
        public string Status { get; set; }
    }
}
