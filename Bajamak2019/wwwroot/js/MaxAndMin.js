﻿let chartConfig = {
    type: 'pie',
    backgroundColor: '#f24c4c',
    borderRadius: '4px',
    plot: {
        tooltip: {
            fontColor: '#333',
            rules: [
                {
                    text: '%v Created',
                    borderRadius: '4px',
                    rule: '%i == 0',
                    shadow: false
                },
                {
                    text: '%v Left',
                    borderRadius: '4px',
                    rule: '%i == 1',
                    shadow: false
                }
            ]
        },
        valueBox: {
            visible: false
        },
        animation: {
            effect: 'ANIMATION_EXPAND_LEFT',
            sequence: 'ANIMATION_BY_PLOT_AND_NODE'
        },
        detach: false,
        hoverState: {
            visible: false
        },
        refAngle: 270,
        slice: '50%'
    },
    plotarea: {
        margin: 'dynamic'
    },
    labels: [
        {
            text: 'CPU 1',
            alpha: 0.7,
            anchor: 'c',
            fontColor: '#fff',
            fontSize: '30px',
            x: '50%',
            y: '62%'
        },
        {
            text: '30',
            fontColor: 'white',
            fontFamily: 'Avenir',
            fontSize: '70px',
            offsetX: '-15px',
            textAlign: 'center',
            width: '100%',
            y: '36%'
        },
        {
            text: '%',
            alpha: 0.8,
            fontColor: 'white',
            fontSize: '40px',
            offsetX: '40px',
            textAlign: 'center',
            width: '100%',
            fontFamily: 'Avenir',
            y: '38%'
        }
    ],
    valueBox: {
        visible: true
    },
    series: [
        {
            values: [30],
            backgroundColor: '#fff',
            borderWidth: '0px',
            fillType: 'radial',
            gradientColors: 'white white #ff9b99 white',
            gradientStops: '0.2 0.5 0.5',
            shadow: false
        },
        {
            values: [70],
            valueBox: {
                visible: false
            },
            backgroundColor: '#fff',
            borderWidth: '1px',
            shadow: false,
            slice: 155
        }
    ]
};

zingchart.render({
    id: 'myChart1',
    data: chartConfig
});