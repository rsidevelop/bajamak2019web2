﻿var app = new Vue({
    el: '#app',
    data: {
        ordenesTijuana: [],  
        ordenesMexicali: [],  
       
    },
    mounted() {
        this.getordenesTijuana();
        this.getordenesMexicali();
    },
    methods: {
        getordenesTijuana() {
            axios.get('/api/Ordenes/1')
                .then(response => {
                    this.ordenesTijuana = response.data;
                });
        },
        getordenesMexicali() {
            axios.get('/api/Ordenes/2')
                .then(response => {
                    this.ordenesMexicali = response.data;
                });
        }
    },
    computed: {

    }
});